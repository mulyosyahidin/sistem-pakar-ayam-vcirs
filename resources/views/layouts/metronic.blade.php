<!DOCTYPE html>
<!--
   Author: Keenthemes
   Product Name: Metronic
   Product Version: 8.2.1
   Purchase: https://1.envato.market/EA4JP
   Website: http://www.keenthemes.com
   Contact: support@keenthemes.com
   Follow: www.twitter.com/keenthemes
   Dribbble: www.dribbble.com/keenthemes
   Like: www.facebook.com/keenthemes
   License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
   -->
<html lang="en" data-bs-theme="light">
<!--begin::Head-->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <title>@yield('title')</title>

    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700">
    <!--end::Fonts-->

    <!--begin::Vendor Stylesheets(used for this page only)-->
    <link href="{{ asset('assets/themes/metronic/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}"
        rel="stylesheet">
    <link href="{{ asset('assets/themes/metronic/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/themes/metronic/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet">
    <!--end::Vendor Stylesheets-->

    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="{{ asset('assets/themes/metronic/plugins/global/plugins.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/themes/metronic/css/style2.bundle.css') }}" rel="stylesheet">
    <!--end::Global Stylesheets Bundle-->

    @yield('custom_head')

    <style>
        [data-bs-theme=light] body:not(.app-blank) {
            background-image: url('{{ asset('assets/themes/metronic/media/patterns/header-bg.jpg') }}')
        }

        [data-bs-theme=dark] body:not(.app-blank) {
            background-image: url({{ asset('assets/themes/metronic/media/patterns/header-bg-dark.jpg') }})
        }
    </style>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon/favicon-16x16.png') }}">

    <link rel="manifest" href="/site.webmanifest">

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled" cz-shortcut-listen="true"
    data-new-gr-c-s-check-loaded="8.909.0" data-gr-ext-installed="">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;

        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
            } else {
                if (localStorage.getItem("data-bs-theme") !== null) {
                    themeMode = localStorage.getItem("data-bs-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }

            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }

            document.documentElement.setAttribute("data-bs-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Wrapper-->
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <!--begin::Header-->
                @include('layouts.includes.metronic.header')
                <!--end::Header-->

                <!--begin::Toolbar-->
                @yield('breadcrumb')
                <!--end::Toolbar-->
                <!--begin::Container-->
                <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start  container-xxl ">
                    <!--begin::Post-->
                    @yield('content')
                    <!--end::Post-->
                </div>
                <!--end::Container-->
                <!--begin::Footer-->
                @include('layouts.includes.metronic.footer')
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Root-->
    <!--end::Main-->

    <!--begin::Engage modals-->
    <!--end::Engage modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <i class="ki-duotone ki-arrow-up"><span class="path1"></span><span class="path2"></span></i>
    </div>
    <!--end::Scrolltop-->

    <form action="{{ route('logout') }}" method="post" id="logout-form">
        @csrf
    </form>

    @yield('custom_html')

    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(mandatory for all pages)-->
    <script src="{{ asset('assets/themes/metronic/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/themes/metronic/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/themes/metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @if (session()->has('success'))
        <script>
            Swal.fire(
                'Success!',
                `{{ session()->get('success') }}`,
                'success'
            );
        </script>
    @endif

    @if (session()->has('error'))
        <script>
            Swal.fire(
                'Oops...',
                `{{ session()->get('error') }}`,
                'error'
            );
        </script>
    @endif

    <script>
        $('.2select').select2({
            allowClear: true,
        });

        $('#datatable').DataTable({
            dom: '<"d-flex justify-content-between align-items-center"lf>t<"d-flex justify-content-between align-items-center"<"d-flex align-items-center justify-content-center justify-content-md-start"i>p>',
        });
    </script>

    <script>
        let logoutLink = document.querySelector('.logout-link');
        if (logoutLink) {
            logoutLink.addEventListener('click', function(e) {
                e.preventDefault();
                document.getElementById('logout-form').submit();
            });
        }
    </script>
    <!--end::Global Javascript Bundle-->

    @stack('custom_js')
    <!--end::Javascript-->

</body>
<!--end::Body-->

</html>
