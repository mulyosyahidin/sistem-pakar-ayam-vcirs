<div class="footer py-4 d-flex flex-lg-column " id="kt_footer">
    <!--begin::Container-->
    <div class=" container-xxl  d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-gray-900 order-2 order-md-1">
            <span class="text-muted fw-semibold me-1">2023 ©</span>
            <a href="{{ route('home') }}" class="text-gray-800 text-hover-primary">Sistem Pakar Diagnosa Penyakit Ayam</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Menu-->
        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
            <li class="menu-item">Desi Ade Winda</li>
        </ul>
        <!--end::Menu-->
    </div>
    <!--end::Container-->
</div>
