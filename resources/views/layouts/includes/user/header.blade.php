<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">

    <!--begin::Menu wrapper-->
    <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true"
        data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}"
        data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start"
        data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true"
        data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
        data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
        <!--begin::Menu-->
        <div class="menu  menu-rounded menu-active-bg menu-state-primary menu-column menu-lg-row menu-title-gray-700 menu-icon-gray-500 menu-arrow-gray-500 menu-bullet-gray-500 my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
            id="kt_app_header_menu" data-kt-menu="true">
            <!--begin:Menu item-->
            <div class="menu-item {{ activeClass('user.dashboard') }} menu-here-bg me-0 me-lg-2">
                <!--begin:Menu link-->
                <span class="menu-link">
                    <a href="{{ route('user.dashboard') }}">
                        <span class="menu-title">
                            Dashboard
                        </span>
                    </a>
                </span>
                <!--end:Menu link-->
            </div>
            <!--end:Menu item-->

            <!--begin:Menu item-->
            <div class="menu-item {{ activeClass('user.diagnosa.*') }} menu-here-bg me-0 me-lg-2">
                <!--begin:Menu link-->
                <span class="menu-link">
                    <a href="{{ route('user.diagnosa.index') }}">
                        <span class="menu-title">
                            Riwayat Diagnosa
                        </span>
                    </a>
                </span>
                <!--end:Menu link-->
            </div>
            <!--end:Menu item-->
        </div>
        <!--end::Menu-->
    </div>
    <!--end::Menu wrapper-->

    <!--begin::Navbar-->
    <div class="app-navbar flex-shrink-0">

        <!--begin::User menu-->
        <div class="app-navbar-item ms-3 ms-lg-5" id="kt_header_user_menu_toggle">
            <!--begin::Menu wrapper-->
            <div class="cursor-pointer symbol symbol-35px symbol-md-45px"
                data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent"
                data-kt-menu-placement="bottom-end">
                <img class="symbol symbol-circle symbol-35px symbol-md-45px" src="{{ auth()->user()->getAvatar() }}"
                    alt="user" />
            </div>

            <!--begin::User account menu-->
            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                data-kt-menu="true">
                <!--begin::Menu item-->
                <div class="menu-item px-3">
                    <div class="menu-content d-flex align-items-center px-3">
                        <!--begin::Avatar-->
                        <div class="symbol symbol-50px me-5">
                            <img alt="Logo" src="{{ auth()->user()->getAvatar() }}" />
                        </div>
                        <!--end::Avatar-->

                        <!--begin::Username-->
                        <div class="d-flex flex-column">
                            <div class="fw-bold d-flex align-items-center fs-5">
                                {{ auth()->user()->name }}
                            </div>

                            <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">
                                {{ auth()->user()->email }} </a>
                        </div>
                        <!--end::Username-->
                    </div>
                </div>
                <!--end::Menu item-->

                <!--begin::Menu separator-->
                <div class="separator my-2"></div>
                <!--end::Menu separator-->

                <!--begin::Menu item-->
                <div class="menu-item px-5">
                    <a href="{{ route('profile.index') }}" class="menu-link px-5">
                        Profile
                    </a>
                </div>
                <!--end::Menu item-->

                <!--begin::Menu item-->
                <div class="menu-item px-5">
                    <a href="#" class="menu-link px-5 logout-link">
                        Sign Out
                    </a>
                </div>
                <!--end::Menu item-->
            </div>
            <!--end::User account menu-->

            <!--end::Menu wrapper-->
        </div>
        <!--end::User menu-->

        <!--begin::Header menu toggle-->
        <!--end::Header menu toggle-->
    </div>
    <!--end::Navbar-->
</div>
