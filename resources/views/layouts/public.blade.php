<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>

    <!-- Pe-7 icon -->
    <link href="{{ asset('assets/themes/nody/css/pe-icon-7.css') }}" rel="stylesheet" type="text/css" />

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('assets/themes/nody/css/owl.carousel.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/themes/nody/css/owl.theme.default.min.css') }}" />

    <!-- css -->
    <link href="{{ asset('assets/themes/nody/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/themes/nody/css/style.min.css') }}" rel="stylesheet" type="text/css" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon/favicon-16x16.png') }}">

    <link rel="manifest" href="/site.webmanifest">

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark align-items-center">
        <div class="container">
            <a class="navbar-brand" href="layout-one-1.html"><img
                    src="{{ asset('assets/images/logo.png') }}" alt="" width="80"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <i data-feather="menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="list-inline ms-auto menu-social-icon mb-0 py-2 py-lg-0">

                    @if (auth()->check())
                        <li class="list-inline-item mr-0">
                            @if (auth()->user()->role == 'admin')
                                <a href="{{ route('admin.dashboard') }}" class="btn btn-light">Dashboard</a>
                            @else
                                <a href="{{ route('user.dashboard') }}" class="btn btn-light">Dashboard</a>
                            @endif
                        </li>
                    @else
                        <li class="list-inline-item mr-0">
                            <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
                        </li>
                        <li class="list-inline-item mr-0">
                            <a href="{{ route('register') }}" class="btn btn-primary">Daftar</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->

    @yield('content')

    <!-- javascript -->
    <script src="{{ asset('assets/themes/nody/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/themes/nody/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/themes/nody/js/jquery.easing.min.js') }}"></script>

    <!-- feather icons -->
    <script src="https://unpkg.com/feather-icons"></script>

    <!-- Main Js -->
    <script src="{{ asset('assets/themes/nody/js/app.js') }}"></script>

</body>

</html>
