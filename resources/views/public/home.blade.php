@extends('layouts.public')
@section('title', 'Selamat Datang di Sistem Pakar Diagnosa Penyakit Ayam')

@section('content')
    <!-- Hero Start -->
    <section class="hero-1-bg bg-light"
        style="background-image: url('{{ asset('assets/themes/nody/images/hero-1-bg-img.png') }})" id="home">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6">
                    <h1 class="display-4 fw-medium mb-4">Halo,</h1>
                    <h1 class="hero-1-title fw-normal text-dark mb-4">Selamat Datang!</h1>
                    <p class="text-muted mb-4 pb-3">Selamat datang di Sistem Pakar Diagnosa Penyakit Ayam dengan Metode
                        VCIRS.</p>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class=" mt-5 mt-lg-0">
                        <img src="{{ asset('assets/images/9027430-bg.png') }}" alt=""
                            class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero End -->
@endsection
