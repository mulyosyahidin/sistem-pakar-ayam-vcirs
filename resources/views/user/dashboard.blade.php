@extends('layouts.user')
@section('title', 'User Dashboard')

@section('toolbar')
    <div id="kt_app_toolbar" class="app-toolbar  py-6 ">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex align-items-start ">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="{{ route('home') }}" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-white fs-3"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            Dashboard</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper--->

                <!--begin::Toolbar wrapper--->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
                    <!--begin::Page title-->
                    <div class="page-title d-flex align-items-center me-3">
                        <img alt="Logo" src="{{ asset('assets/images/icon.png') }}"
                            class="h-60px me-5" />
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                            User Dashboard
                            <!--begin::Description-->
                            <span class="page-desc text-white opacity-50 fs-6 fw-bold">
                                Sistem Pakar Diagnosa Penyakit Ayam
                            </span>
                            <!--end::Description-->
                        </h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper--->
            </div>
            <!--end::Toolbar container--->
        </div>
        <!--end::Toolbar container-->
    </div>
@endsection

@section('content')
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content ">
            <!--begin::Content menu-->
            <div
                class="app-content-menu menu menu-rounded menu-gray-800 menu-state-bg flex-wrap fs-5 fw-semibold border-bottom mt-n7 pt-5 pb-6 px-10 mb-10">
                <!--begin::Menu item-->
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 active" href>
                        <span class="menu-title text-gray-800">Diagnosa Terbaru</span>
                    </a>
                </div>
                <!--end::Menu item-->
            </div>
            <!--begin::Content menu-->
            <!--begin::Row-->
            <div class="row gx-5 gx-xl-10">
                <div class="col-12">
                    <div class="card card-flush h-md-100">
                        <!--begin::Body-->
                        <div class="card-body pt-6">
                            <!--begin::Table container-->
                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table table-row-dashed align-middle gs-0 gy-3 my-0" id="datatable">
                                    <!--begin::Table head-->
                                    <thead>
                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                            <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Hasil</th>
                                            <th>Persentase Keyakinan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="text-black-300 fw-semibold">
                                        @forelse ($riwayatDiagnosa as $diagnosa)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td class="pe-0">
                                                    <span>{{ $diagnosa->created_at->translatedFormat('l, d M Y H:i') }}</span>
                                                </td>
                                                <td>
                                                    <span>{{ $diagnosa->penyakit?->nama }}</span>
                                                </td>
                                                <td>
                                                    <span>{{ $diagnosa->persentase }}%</span>
                                                </td>
                                                <td class="text-end">
                                                    <a href="{{ route('user.diagnosa.show', $diagnosa) }}"
                                                        class="btn btn-sm btn-success">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">Tidak ada data</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Content wrapper-->
@endsection
