@extends('layouts.user')
@section('title', 'Data Diagnosa')

@section('toolbar')
    <div id="kt_app_toolbar" class="app-toolbar  py-6 ">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex align-items-start ">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="{{ route('home') }}" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-white fs-3"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            Data Diagnosa</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper--->

                <!--begin::Toolbar wrapper--->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-8">
                    <!--begin::Page title-->
                    <div class="page-title d-flex align-items-center me-3 justify-content-between">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                            Data Diagnosa
                        </h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                    <!--begin::Items-->
                    <div class="d-flex gap-4 gap-lg-13">
                        <!--begin::Item-->
                        <div class="d-flex flex-column">
                            <!--begin::Number-->
                            <a href="{{ route('user.diagnosa.index') }}" class="btn btn-sm btn-white">Kembali</a>
                            <!--end::Number-->
                        </div>
                    </div>
                </div>
                <!--end::Toolbar wrapper--->
            </div>
            <!--end::Toolbar container--->
        </div>
        <!--end::Toolbar container-->
    </div>
@endsection

@section('content')
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content p-0 mb-5">
            <!--begin::Row-->
            <div class="row gx-5 gx-xl-10">
                <!--begin::Col-->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body py-3">
                            <!--begin::Table container-->
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 fw-semibold gy-4">
                                    <tbody>
                                        <tr class="even">
                                            <td class="text-gray-600">Tanggal</td>
                                            <td class="text-black-600">
                                                {{ $diagnosa->created_at->translatedFormat('l, d M Y H:i') }}</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="text-gray-600">Penyakit Terdiagnosa</td>
                                            <td class="text-black-600">{{ $diagnosa->penyakit->nama }}</td>
                                        </tr>
                                        <tr class="even">
                                            <td class="text-gray-600">Persentase Keyakinan</td>
                                            <td class="text-black-600">{{ $diagnosa->persentase }}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table container-->
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <a href="#" class="btn btn-danger btn-sm delete-btn">Hapus</a>
                        </div>
                    </div>
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row-->
        </div>
        <!--end::Content-->

        <!--begin::Content-->
        <div class="row mb-5">
            <div class="col-12">
                <div class="card mt-3">
                    <div class="card card-flush h-md-100">
                        <!--begin::Header-->
                        <div class="card-header  border-0 pt-6">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bold text-gray-800">Data Gejala</span>
                            </h3>
                            <!--end::Title-->
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-6">
                            <!--begin::Table container-->
                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table table-row-dashed align-middle gs-0 gy-3 my-0" id="datatable">
                                    <!--begin::Table head-->
                                    <thead>
                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                            <th>#</th>
                                            <th>Nama Gejala</th>
                                        </tr>
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="text-black-300 fw-semibold">
                                        @foreach ($diagnosa->gejala as $gejala)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $gejala->nama }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card mt-3">
                    <div class="card card-flush h-md-100">
                        <!--begin::Header-->
                        <div class="card-header  border-0 pt-6">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bold text-gray-800">Data Solusi</span>
                            </h3>
                            <!--end::Title-->
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-6">
                            <!--begin::Table container-->
                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table table-row-dashed align-middle gs-0 gy-3 my-0" id="datatable">
                                    <!--begin::Table head-->
                                    <thead>
                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                            <th>#</th>
                                            <th>Solusi</th>
                                        </tr>
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="text-black-300 fw-semibold">
                                        @foreach ($diagnosa->penyakit->solusi as $solusi)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $solusi->solusi }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('user.diagnosa.destroy', $diagnosa) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtn = document.querySelector('.delete-btn');

        deleteBtn.addEventListener('click', function() {
            Swal.fire({
                title: 'Hapus Data?',
                text: "Yakin ingin menghapus data diagnosa ini? Tindakan ini tidak dapat dibatalkan.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Hapus',
            }).then((result) => {
                if (result.isConfirmed) {
                    let form = document.querySelector('#delete-form');

                    form.submit();
                }
            })
        });
    </script>
@endpush
