@extends('layouts.user')
@section('title', 'Buat Diagnosa Baru')

@section('custom_head')
    <style>
        .checkbox {
            display: none;
        }

        .checkbox .gejala h6 {
            font-size: 10px;
            font-weight: 400;
            margin-top: -5px;
        }

        .gejala {
            background: #fff;
            border-radius: 10px;
            padding: 20px;
            border: 1px solid #e9ecef;
            cursor: pointer;
        }

        .checkbox:checked+.gejala {
            background: #217AFF;
        }

        .checkbox:checked+.gejala h5 {
            color: #fff;
        }

        .checkbox:checked+.gejala h6 {
            color: #e9ecef;
        }

        .checkbox .gejala {
            display: block;
            height: 100%;
            width: 100%;
            cursor: pointer;

        }

        .showing-list {
            margin-bottom: 15px;
        }

        .for-checkbox {
            padding: 0;
        }

        .for-checkbox img {
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        .for-checkbox iframe {
            width: 100%;
            height: 250px;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        .for-checkbox img,
        .for-checkbox a {
            width: 100%;
            height: 250px;
            display: block;
        }

        .checkbox:checked+.for-checkbox a {
            color: #fff;
        }

        .for-checkbox h5 {
            font-size: 16px;
            line-height: 15px
        }

        .for-checkbox h6 {
            font-size: 13px;
        }

        .for-checkbox .mb-1 {
            position: relative;
        }

        .for-checkbox .mb-1 img,
        .for-checkbox .mb-1 iframe,
        .for-checkbox .mb-1 div a {
            display: block;
        }

        .for-checkbox .mb-1 span {
            position: absolute;
            bottom: 0;
            background-color: rgba(0, 0, 0, 0.7);
            color: white;
            padding: 5px;
            width: 100%;
        }

        .gejala img {
            width: 100%;
            object-fit: cover;
        }
    </style>
@endsection

@section('toolbar')
    <div id="kt_app_toolbar" class="app-toolbar  py-6 ">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex align-items-start ">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="{{ route('home') }}" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-white fs-3"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="{{ route('user.diagnosa.index') }}" class="text-white">Diagnosa</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            Buat Baru
                        </li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper--->

                <!--begin::Toolbar wrapper--->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-8">
                    <!--begin::Page title-->
                    <div class="page-title d-flex align-items-center me-3 justify-content-between">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                            Buat Diagnosa Baru
                        </h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                    <!--begin::Items-->
                    <div class="d-flex gap-4 gap-lg-13">
                        <!--begin::Item-->
                        <div class="d-flex flex-column">
                            <!--begin::Number-->
                            <a href="{{ route('user.diagnosa.index') }}" class="btn btn-sm btn-white">Kembali</a>
                            <!--end::Number-->
                        </div>
                    </div>
                </div>
                <!--end::Toolbar wrapper--->
            </div>
            <!--end::Toolbar container--->
        </div>
        <!--end::Toolbar container-->
    </div>
@endsection

@section('content')
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content p-0 mb-5">
            <!--begin::Row-->
            <div class="row gx-5 gx-xl-10">
                <!--begin::Col-->
                <div class="col-12">
                    <form action="{{ route('user.diagnosa.store') }}" method="POST">
                        @csrf

                        <!--begin::Tables widget 14-->
                        <div class="card">
                            <!--begin::Header-->
                            <div class="card-header  border-0 pt-6">
                                <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                                    @foreach ($gejala as $item)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $loop->index == 0 ? 'active' : '' }}" data-bs-toggle="tab"
                                                href="#tab_{{ $loop->index }}">{{ $item['kategori'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--end::Header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0">
                                <div class="tab-content" id="myTabContent">
                                    @foreach ($gejala as $item)
                                        <div class="tab-pane fade {{ $loop->index == 0 ? 'show active' : '' }}"
                                            id="tab_{{ $loop->index }}" role="tabpanel">
                                            <div class="row">
                                                @foreach ($item['data'] as $data)
                                                    <div class="col-12 col-md-4 mb-3 d-flex">
                                                        <input class="checkbox pilih-gejala" type="checkbox" name="gejala[]"
                                                            id="gejala-{{ $data->id }}" value="{{ $data->id }}" />
                                                        <label class="for-checkbox gejala flex-fill"
                                                            for="gejala-{{ $data->id }}">
                                                            <div class="mb-1">
                                                                @if ($data->media_type == 'image')
                                                                    <img src="{{ asset($data->media_url) }}" alt=""
                                                                        class="img-fluid">
                                                                @elseif ($data->media_type == 'video')
                                                                    <iframe
                                                                        src="{{ getYouTubeEmbedURL($data->media_url) }}"
                                                                        title="YouTube video player" frameborder="0"
                                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                                        allowfullscreen></iframe>
                                                                @else
                                                                    <img src="https://placehold.co/1920x1080?text=No Image"
                                                                        alt="" class="img-fluid no-image">
                                                                @endif
                                                            </div>
                                                            <div class="px-3 pt-3">
                                                                <h5>{{ $data->nama }}</h5>
                                                                <h6>{{ $data->kode }}</h6>
                                                            </div>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!--end::Card body-->
                            <!--begin::Footer-->
                            <div class="card-footer d-flex justify-content-end py-6 px-9">
                                <button type="submit" class="btn btn-primary" id="kt_forms_widget_14_submit_button">Lakukan
                                    Diagnosa</button>
                            </div>
                            <!--end::Footer-->
                        </div>
                        <!--end::Tables widget 14-->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom_js')
    <script src="{{ asset('assets/themes/metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>

    <script>
        $('#kt_subscriptions_table').DataTable({
            paging: false,
            language: {
                search: ""
            },
            dom: '<"d-flex justify-content-between align-items-center"f><"d-flex justify-content-between align-items-center"<"d-flex align-items-center justify-content-center justify-content-md-start">>',
        });
    </script>
@endpush
