@extends('layouts.user')
@section('title', 'Kelola Data Diagnosa')

@section('toolbar')
    <div id="kt_app_toolbar" class="app-toolbar  py-6 ">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex align-items-start ">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="{{ route('home') }}" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-white fs-3"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            Diagnosa</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper--->

                <!--begin::Toolbar wrapper--->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-8">
                    <!--begin::Page title-->
                    <div class="page-title d-flex align-items-center me-3 justify-content-between">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                            Kelola Data Diagnosa
                        </h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                    <!--begin::Items-->
                    <div class="d-flex gap-4 gap-lg-13">
                        <!--begin::Item-->
                        <div class="d-flex flex-column">
                            <!--begin::Number-->
                            <a href="{{ route('user.diagnosa.create') }}" class="btn btn-sm btn-white">Diagnosa Baru</a>
                            <!--end::Number-->
                        </div>
                    </div>
                </div>
                <!--end::Toolbar wrapper--->
            </div>
            <!--end::Toolbar container--->
        </div>
        <!--end::Toolbar container-->
    </div>
@endsection

@section('content')
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content p-0 mb-5">
            <!--begin::Row-->
            <div class="row gx-5 gx-xl-10">
                <!--begin::Col-->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body py-3">
                            <!--begin::Table container-->
                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table table-row-dashed align-middle gs-0 gy-4 table-hover">
                                    <!--begin::Table head-->
                                    <thead>
                                        <tr class="fs-7 fw-bold border-0 text-gray-500">
                                            <th>#</th>
                                            <th class="min-w-150px pe-0">Tanggal</th>
                                            <th class="min-w-150px">Hasil</th>
                                            <th class="min-w-150px">Persentase Keyakinan</th>
                                            <th class="min-w-150px"></th>
                                        </tr>
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody>
                                        @forelse ($data as $item)
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800 fw-bold fs-6 me-1">
                                                        {{ $loop->iteration }}
                                                    </span>
                                                </td>
                                                <td class="pe-0">
                                                    <span>{{ $item->created_at->translatedFormat('l, d M Y H:i') }}</span>
                                                </td>
                                                <td class="pe-0">
                                                    <span>{{ $item->penyakit->nama }}</span>
                                                </td>
                                                <td class="pe-0">
                                                    <span>{{ $item->persentase }}%</span>
                                                </td>
                                                <td class="text-end">
                                                    <a href="{{ route('user.diagnosa.show', $item) }}"
                                                        class="btn btn-sm btn-success">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a href="#" data-id="{{ $item->id }}"
                                                        class="btn btn-sm btn-danger delete-btn">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Tidak ada data</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                            <!--end::Table container-->
                        </div>
                        @if ($data->hasPages())
                            <div class="card-footer pb-0">
                                {{ $data->links() }}
                            </div>
                        @endif
                    </div>
                </div>
                <!--end::Col-->
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="#" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtns = document.querySelectorAll('.delete-btn');

        deleteBtns.forEach((btn) => {
            let id = btn.getAttribute('data-id');

            btn.addEventListener('click', function() {
                Swal.fire({
                    title: 'Hapus Data?',
                    text: "Yakin ingin menghapus data? Tindakan ini tidak dapat dibatalkan.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Hapus',
                }).then((result) => {
                    if (result.isConfirmed) {
                        let form = document.querySelector('#delete-form');

                        form.setAttribute(`action`,
                            `{{ route('user.diagnosa.destroy', false) }}/${id}`);
                        form.submit();
                    }
                })
            });
        });
    </script>
@endpush
