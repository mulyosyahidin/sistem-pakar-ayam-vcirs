@extends('layouts.metronic')
@section('title', 'Edit Data Basis Pengetahuan')

@section('breadcrumb')
    <div class="toolbar py-5 pb-lg-15" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class=" container-xxl  d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-white fw-bold my-1 fs-3">
                    Edit Data Basis Pengetahuan
                </h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.dashboard') }}" class="text-white text-hover-primary">
                            Dashboard </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.penyakit.index') }}" class="text-white text-hover-primary">
                            Penyakit </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.penyakit.show', $penyakit) }}" class="text-white text-hover-primary">
                            {{ $penyakit->nama }} </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        Basis Pengetahuan
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-3 py-md-1 gap-2">
                <!--begin::Button-->
                <a href="{{ route('admin.basis-pengetahuan.index') }}" data-bs-theme="light"
                    class="btn bg-body btn-active-color-primary">
                    Kembali </a>
                <!--end::Button-->
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('content')
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row g-5 g-xl-8">
            <div class="col-12">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Data Gejala</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Card header-->

                    <form action="{{ route('admin.basis-pengetahuan.update', $penyakit) }}" method="post">
                        @csrf
                        @method('PUT')

                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table-->
                            <div id="kt_subscriptions_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-hover"
                                        id="kt_subscriptions_table">
                                        <thead>
                                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 29.9px;">
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                        <input class="form-check-input" type="checkbox" data-kt-check="true"
                                                            data-kt-check-target="#kt_subscriptions_table .form-check-input"
                                                            value="1">
                                                    </div>
                                                </th>
                                                <th>Kode</th>
                                                <th>Gejala</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-gray-600 fw-semibold">
                                            @foreach ($gejala as $item)
                                                <tr class="{{ $loop->index % 2 == 0 ? 'odd' : 'even' }}">
                                                    <td>
                                                        <div
                                                            class="form-check form-check-sm form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="checkbox" name="gejala[]"
                                                                value="{{ $item->id }}"
                                                                {{ old('gejala') && in_array($item->id, old('gejala')) ? 'checked' : (in_array($item->id, $gejalaPenyakit) ? 'checked' : '') }}>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span
                                                            class="text-gray-800 text-hover-primary mb-1">{{ $item->kode }}</span>
                                                    </td>
                                                    <td>
                                                        {{ $item->nama }} </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Card body-->

                        <!--begin::Card footer-->
                        <div class="card-footer">
                            <div class="d-flex justify-content-end">
                                <input type="submit" value="Simpan" class="btn btn-primary">
                            </div>
                        </div>
                        <!--end::Card footer-->
                    </form>
                </div>
            </div>
        </div>
        <!--end::Row-->
    </div>
@endsection
