@extends('layouts.metronic')
@section('title', 'Hasil Diagnosa')

@section('breadcrumb')
    <div class="toolbar py-5 pb-lg-15" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class=" container-xxl  d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-white fw-bold my-1 fs-3">
                    Hasil Diagnosa
                </h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.dashboard') }}" class="text-white text-hover-primary">
                            Dashboard </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.diagnosa.index') }}" class="text-white text-hover-primary">
                            Diagnosa </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        Hasil Diagnosa
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('content')
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row g-5 g-xl-8">
            <div class="col-12">
                <div class="card card-flush pt-3 mb-5">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <h2 class="fw-bold">Data Diagnosa</h2>
                        </div>
                        <!--begin::Card title-->
                    </div>
                    <!--end::Card header-->
    
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table wrapper-->
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 fw-semibold gy-4 dataTable no-footer">
                                    <tbody>
                                        <tr class="even">
                                            <td class="text-gray-600">User</td>
                                            <td class="text-black-600">{{ $diagnosa->user->name }}</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="text-gray-600">Hasil Diagnosa</td>
                                            <td class="text-black-600">{{ $diagnosa->penyakit->nama }}</td>
                                        </tr>
                                        <tr class="even">
                                            <td class="text-gray-600">Persentase</td>
                                            <td class="text-black-600">{{ $diagnosa->persentase }}%</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="text-gray-600">Jumlah Gejala</td>
                                            <td class="text-black-600">{{ $diagnosa->gejala_count }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Table wrapper-->
                    </div>
                    <!--end::Card body-->
    
                    <!--begin::Card footer-->
                    <div class="card-footer">
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-danger btn-sm btn-delete ms-2">
                                <i class="bi bi-trash"></i>
                                Hapus
                            </button>
                        </div>
                    </div>
                    <!--end::Card footer-->
                </div>
                
                <div class="card card-flush">
                    <!--begin::Header-->
                    <div class="card-header  border-0 pt-6">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Bobot Nilai CF Gejala Penyakit</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <!--begin::Table container-->
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table table-row-dashed align-middle gs-0 gy-3 my-0 table-hover table-bordered"
                                id="datatable">
                                <!--begin::Table head-->
                                <thead>
                                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Gejala</th>
                                        <th colspan="{{ $dataPenyakit->count() }}" class="text-center">Penyakit</th>
                                    </tr>
                                    <tr>
                                        @foreach ($dataPenyakit as $item)
                                            <td class="text-center">{{ $item->kode }}</td>
                                        @endforeach
                                    </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="text-black-300 fw-semibold">
                                    @foreach ($diagnosaService->matrixNilaiCf() as $kodeGejala => $data)
                                        <tr>
                                            <td class="text-center">{{ $kodeGejala }}</td>
                                            @foreach ($data as $nilai)
                                                <td class="text-center">
                                                    {{ $nilai }}
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end: Card Body-->
                </div>
            </div>

            <div class="col-12">
                <div class="card card-flush h-md-100">
                    <!--begin::Header-->
                    <div class="card-header  border-0 pt-6">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Gejala Yang Dipilih</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <!--begin::Table container-->
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table table-row-dashed align-middle gs-0 gy-3 my-0 table-hover" id="datatable">
                                <!--begin::Table head-->
                                <thead>
                                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                        <th>#</th>
                                        <th>Kode</th>
                                        <th>Gejala</th>
                                        <th>Bobot</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="text-black-300 fw-semibold">
                                    @foreach ($diagnosa->gejala as $gejala)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $gejala->kode }}</td>
                                            <td>{{ $gejala->nama }}</td>
                                            <td>{{ $gejala->nilai_cf }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end: Card Body-->
                </div>
            </div>

            <div class="col-12">
                <!--begin::Tables widget 14-->
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Perhitungan VCIRS</h3>
                        </div>
                        <!--end::Card title-->
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <div class="accordion" id="perhitungan">
                            @foreach ($diagnosaService->hitung() as $data)
                                <h2 class="accordion-header" id="heading-{{ $loop->index }}">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-{{ $loop->index }}"
                                        aria-expanded="{{ $loop->index == 0 ? 'true' : 'false' }}"
                                        aria-controls="collapse-{{ $loop->index }}">
                                        {{ $data['penyakit']->nama }}
                                    </button>
                                </h2>
                                <div id="collapse-{{ $loop->index }}"
                                    class="accordion-collapse collapse {{ $loop->index == 0 ? 'show' : '' }}"
                                    aria-labelledby="heading-{{ $loop->index }}" data-bs-parent="#perhitungan">
                                    <div class="accordion-body">
                                        <h5>Rule</h5>

                                        <!--begin::Table-->
                                        <div id="kt_subscriptions_table_wrapper_{{ $loop->index }}_1"
                                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-hover datatable">
                                                    <thead>
                                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                            <th>Kode Gejala</th>
                                                            <th>Nama Gejala</th>
                                                            <th>Jumlah Node</th>
                                                            <th>Node yang menggunakan</th>
                                                            <th class="text-center">Urutan Node</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-gray-600 fw-semibold">
                                                        @foreach ($data['rule'] as $rule)
                                                            <tr>
                                                                <td>{{ $rule['gejala']->kode }}</td>
                                                                <td>{{ $rule['gejala']->nama }}</td>
                                                                <td class="text-center">{{ $rule['number_of_node'] }}</td>
                                                                <td>
                                                                    @foreach ($rule['nodes'] as $node)
                                                                        <span class="badge badge-light-success">
                                                                            P0{{ $node }}#{{ $node }}
                                                                        </span>
                                                                    @endforeach
                                                                </td>
                                                                <td class="text-center">{{ $rule['number'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--end::Table-->

                                        <h5 class="mt-5">Nilai VUR</h5>
                                        <!--begin::Table-->
                                        <div id="kt_subscriptions_table_wrapper_{{ $loop->index }}_2"
                                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-hover datatable">
                                                    <thead>
                                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                            <th>Kode Gejala</th>
                                                            <th>Nama Gejala</th>
                                                            <th class="text-center">Nilai VUR</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-gray-600 fw-semibold">
                                                        @foreach ($data['vur'] as $vur)
                                                            <tr>
                                                                <td>{{ $vur['gejala']->kode }}</td>
                                                                <td>{{ $vur['gejala']->nama }}</td>
                                                                <td class="text-center">{{ $vur['nilai'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--end::Table-->

                                        <h5 class="mt-5">Nilai NUR</h5>
                                        <p>Nilai NUR didapatkan: <strong>{{ $data['nur'] }}</strong></p>

                                        <h5 class="mt-5">Nilai RUR</h5>
                                        <p>Nilai RUR didapatkan: <strong>{{ $data['rur'] }}</strong></p>

                                        <h5 class="mt-5">Perhitungan Nilai CF</h5>

                                        <h6>Jawaban Quisioner Terhadap Gejala Penyakit</h6>
                                        <!--begin::Table-->
                                        <div id="kt_subscriptions_table_wrapper_{{ $loop->index }}_3"
                                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-hover datatable">
                                                    <thead>
                                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                            <th>Kode Gejala</th>
                                                            <th>Nama Gejala</th>
                                                            <th class="text-center">Jawaban User</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-gray-600 fw-semibold">
                                                        @foreach ($data['jawaban_user'] as $jawaban)
                                                            <tr>
                                                                <td>{{ $jawaban['gejala']->kode }}</td>
                                                                <td>{{ $jawaban['gejala']->nama }}</td>
                                                                <td class="text-center">{{ $jawaban['jawaban'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--end::Table-->

                                        <h6 class="mt-2">Nilai CF</h6>
                                        <!--begin::Table-->
                                        <div id="kt_subscriptions_table_wrapper_{{ $loop->index }}_3"
                                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-hover table-bordered">
                                                    <thead>
                                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                            <th rowspan="2" class="text-center"
                                                                style="vertical-align: middle;">Kode Gejala</th>
                                                            <th rowspan="2" style="vertical-align: middle;">Nama Gejala
                                                            </th>
                                                            <th colspan="2" class="text-center"
                                                                style="vertical-align: middle;">Bobot</th>
                                                            <th colspan="2" class="text-center"
                                                                style="vertical-align: middle;">Nilai</th>
                                                        </tr>
                                                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                            <td class="text-center">Pakar</td>
                                                            <td class="text-center">User</td>
                                                            <td class="text-center">CF</td>
                                                            <td class="text-center">CFR</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-gray-600 fw-semibold">
                                                        @foreach ($data['nilai_cf'] as $nilaiCf)
                                                            <tr>
                                                                <td class="text-center">{{ $nilaiCf['gejala']->kode }}
                                                                </td>
                                                                <td>{{ $nilaiCf['gejala']->nama }}</td>
                                                                <td class="text-center">{{ $nilaiCf['bobot']['pakar'] }}
                                                                </td>
                                                                <td class="text-center">{{ $nilaiCf['bobot']['user'] }}
                                                                </td>
                                                                <td class="text-center">{{ $nilaiCf['nilai']['cf'] }}
                                                                </td>
                                                                <td class="text-center">{{ $nilaiCf['nilai']['cfr'] }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--end::Table-->

                                        <h5 class="mt-5">Nilai CF Kombinasi</h5>
                                        <p>
                                            Berdasarkan hasil perhitungan, penyakit <b>{{ $data['penyakit']['nama'] }}</b>
                                            menghasilkan nilai {{ $data['cf_kombinasi']['result'] }} atau
                                            {{ $data['cf_kombinasi']['persentase'] }}% keyakinan.
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Tables widget 14-->
            </div>

            <div class="col-12">
                <div class="card card-flush h-md-100">
                    <!--begin::Header-->
                    <div class="card-header  border-0 pt-6">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Hasil</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <p>
                            Berdasarkan hasil perhitungan, gejala-gejala yang dilakukan diagnosa tersebut menghasilkan
                            penyakit <b>{{ $diagnosaService->penyakitTertinggi()['penyakit']['nama'] }}</b> dengan nilai
                            {{ $diagnosaService->penyakitTertinggi()['cf_kombinasi']['result'] }} atau
                            {{ $diagnosaService->penyakitTertinggi()['cf_kombinasi']['persentase'] }}% keyakinan.
                        </p>
                    </div>
                    <!--end: Card Body-->
                </div>
            </div>
        </div>
        <!--end::Row-->
    </div>
@endsection
