@extends('layouts.metronic')
@section('title', 'Edit Data Solusi')

@section('breadcrumb')
    <div class="toolbar py-5 pb-lg-15" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class=" container-xxl  d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-white fw-bold my-1 fs-3">
                    Edit Data Solusi
                </h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.dashboard') }}" class="text-white text-hover-primary">
                            Dashboard </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.solusi.index') }}" class="text-white text-hover-primary">
                            Solusi </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                     <!--begin::Item-->
                     <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.solusi.show', $solusi) }}" class="text-white text-hover-primary">
                            {{ $solusi->kode }} </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        Edit Data
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-3 py-md-1">
                <!--begin::Button-->
                <a href="{{ route('admin.solusi.show', $solusi) }}" data-bs-theme="light"
                    class="btn bg-body btn-active-color-primary">
                    Kembali </a>
                <!--end::Button-->
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('content')
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row g-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-12">
                <form action="{{ route('admin.solusi.update', $solusi) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <!--begin::Tables widget 14-->
                    <div class="card">
                        <!--begin::Card header-->
                        <div class="card-header border-0">
                            <!--begin::Card title-->
                            <div class="card-title m-0">
                                <h3 class="fw-bold m-0">Edit Solusi</h3>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--begin::Card header-->
                        <!--begin::Body-->
                        <div class="card-body pt-6 border-top">
                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Kode</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input type="text" name="kode" value="{{ old('kode', $solusi->kode) }}"
                                    class="form-control @error('kode') is-invalid @enderror" maxlength="4" required>
                                <!--end::Input-->

                                @error('kode')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->

                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Solusi</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <textarea name="solusi" class="form-control @error('solusi') is-invalid @enderror" required>{{ old('solusi', $solusi->solusi) }}</textarea>
                                <!--end::Input-->

                                @error('solusi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end: Card Body-->
                        <!--begin::Footer-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="submit" class="btn btn-primary"
                                id="kt_forms_widget_14_submit_button">Simpan</button>
                        </div>
                        <!--end::Footer-->
                    </div>
                    <!--end::Tables widget 14-->
                </form>
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
@endsection
