@extends('layouts.metronic')
@section('title', 'Edit Data Gejala')

@section('breadcrumb')
    <div class="toolbar py-5 pb-lg-15" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class=" container-xxl  d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-white fw-bold my-1 fs-3">
                    Edit Data Gejala
                </h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.dashboard') }}" class="text-white text-hover-primary">
                            Dashboard </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.gejala.index') }}" class="text-white text-hover-primary">
                            Gejala </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.gejala.show', $gejala) }}" class="text-white text-hover-primary">
                            {{ $gejala->nama }} </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        Edit Data
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-3 py-md-1">
                <!--begin::Button-->
                <a href="{{ route('admin.gejala.show', $gejala) }}" data-bs-theme="light"
                    class="btn bg-body btn-active-color-primary">
                    Kembali </a>
                <!--end::Button-->
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('content')
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row g-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-12">
                <form action="{{ route('admin.gejala.update', $gejala) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <!--begin::Tables widget 14-->
                    <div class="card">
                        <!--begin::Card header-->
                        <div class="card-header border-0 pt-6">
                            <!--begin::Card title-->
                            <div class="card-title m-0">
                                <h3 class="fw-bold m-0">Edit Gejala</h3>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--begin::Card header-->
                        <!--begin::Body-->
                        <div class="card-body pt-6 border-top">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Kode</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" name="kode" value="{{ old('kode', $gejala->kode) }}"
                                            class="form-control @error('kode') is-invalid @enderror" maxlength="4"
                                            required>
                                        <!--end::Input-->

                                        @error('kode')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <div class="col-12 col-md-6">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Nama</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" name="nama" value="{{ old('nama', $gejala->nama) }}"
                                            class="form-control @error('nama') is-invalid @enderror" required>
                                        <!--end::Input-->

                                        @error('nama')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->
                                </div>
                            </div>

                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Nilai CF Pakar</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input type="text" name="nilai_cf" value="{{ old('nilai_cf', $gejala->nilai_cf) }}"
                                    class="form-control @error('nilai_cf') is-invalid @enderror">
                                <!--end::Input-->

                                @error('nilai_cf')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->

                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Kategori</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <select name="id_kategori" class="form-control @error('id_kategori') is-invalid @enderror">
                                    <option selected disabled>Pilih Kategori</option>
                                    @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}"
                                            {{ old('id_kategori', $gejala->id_kategori) == $item->id ? 'selected' : '' }}>
                                            {{ $item->nama }}
                                        </option>
                                    @endforeach
                                </select>
                                <!--end::Input-->

                                @error('id_kategori')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Tipe Media</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <select name="media_type" id="media-type"
                                            class="form-control @error('media_type') is-invalid @enderror">
                                            <option selected disabled>Pilih Tipe Media</option>
                                            <option value="video"
                                                {{ old('media_type', $gejala->media_type) == 'video' ? 'selected' : '' }}>
                                                Video</option>
                                            <option value="image"
                                                {{ old('media_type', $gejala->media_type) == 'image' ? 'selected' : '' }}>
                                                Foto</option>
                                        </select>
                                        <!--end::Input-->

                                        @error('media_type')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <div class="col-12 col-md-6">
                                    <!--begin::Input group-->
                                    <div
                                        class="fv-row mb-7 media-video {{ $gejala->media_type == 'video' ? '' : 'd-none' }}">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>URL YouTube</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="url" name="media_url"
                                            value="{{ old('media_url', $gejala->media_url) }}"
                                            class="form-control @error('media_url') is-invalid @enderror">
                                        <!--end::Input-->

                                        @error('media_url')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div
                                        class="fv-row mb-7 media-image {{ $gejala->media_type == 'image' ? '' : 'd-none' }}">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Pilih Foto</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="file" name="media_file"
                                            class="form-control @error('media_file') is-invalid @enderror">
                                        <!--end::Input-->

                                        <small class="text-muted">Pilih gambar baru untuk mengganti yang lama.</small>

                                        @error('media_file')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->
                                </div>
                            </div>
                        </div>
                        <!--end: Card Body-->
                        <!--begin::Footer-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="submit" class="btn btn-primary"
                                id="kt_forms_widget_14_submit_button">Simpan</button>
                        </div>
                        <!--end::Footer-->
                    </div>
                    <!--end::Tables widget 14-->
                </form>
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
@endsection

@push('custom_js')
    <script>
        let mediaType = document.getElementById('media-type');
        let mediaVideo = document.querySelector('.media-video');
        let mediaImage = document.querySelector('.media-image');

        mediaType.addEventListener('change', function() {
            if (this.value == 'video') {
                mediaVideo.classList.remove('d-none');
                mediaImage.classList.add('d-none');
            } else if (this.value == 'image') {
                mediaVideo.classList.add('d-none');
                mediaImage.classList.remove('d-none');
            }
        });
    </script>
@endpush
