@extends('layouts.metronic')
@section('title', $penyakit->nama)

@section('breadcrumb')
    <div class="toolbar py-5 pb-lg-15" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class=" container-xxl  d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-white fw-bold my-1 fs-3">
                    Data Penyakit
                </h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.dashboard') }}" class="text-white text-hover-primary">
                            Dashboard </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ route('admin.penyakit.index') }}" class="text-white text-hover-primary">
                            Penyakit </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-white opacity-75">
                        {{ $penyakit->nama }}
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-3 py-md-1">
                <!--begin::Button-->
                <a href="{{ route('admin.penyakit.index') }}" data-bs-theme="light"
                    class="btn bg-body btn-active-color-primary">
                    Kembali </a>
                <!--end::Button-->
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('content')
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row g-5 g-xl-8">
            <div class="card card-flush pt-3 mb-5">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h2 class="fw-bold">{{ $penyakit->nama }}</h2>
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->

                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table wrapper-->
                    <div class="table-responsive">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 fw-semibold gy-4 dataTable no-footer">
                                <tbody>
                                    <tr class="even">
                                        <td class="text-gray-600">Kode</td>
                                        <td class="text-black-600">{{ $penyakit->kode }}</td>
                                    </tr>
                                    <tr class="odd">
                                        <td class="text-gray-600">Nama</td>
                                        <td class="text-black-600">{{ $penyakit->nama }}</td>
                                    </tr>
                                    <tr class="even">
                                        <td class="text-gray-600">Jenis</td>
                                        <td class="text-black-600">{{ $penyakit->jenis }}</td>
                                    </tr>
                                    <tr class="odd">
                                        <td class="text-gray-600">Jumlah Solusi</td>
                                        <td class="text-black-600">{{ $penyakit->solusi_count }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Table wrapper-->
                </div>
                <!--end::Card body-->

                <!--begin::Card footer-->
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('admin.penyakit.edit', $penyakit) }}" class="btn btn-primary">
                            <i class="bi bi-pencil-square"></i>
                            Edit
                        </a>
                        <button class="btn btn-danger btn-delete ms-2">
                            <i class="bi bi-trash"></i>
                            Hapus
                        </button>
                    </div>
                </div>
                <!--end::Card footer-->
            </div>

            <div class="card card-flush h-md-100">
                <!--begin::Header-->
                <div class="card-header  border-0 pt-6">
                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-gray-800">Data Solusi</span>
                    </h3>
                    <!--end::Title-->

                    <a href="{{ route('admin.penyakit.solusi.edit', $penyakit) }}" style="max-height: 35px"
                        class="btn btn-info btn-sm" data-bs-toggle="tooltip" title="Edit Solusi Penyakit"><i
                            class="fa fa-list"></i></a>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-6">
                    <!--begin::Table container-->
                    <div class="table-responsive">
                        <!--begin::Table-->
                        <table class="table table-row-dashed align-middle gs-0 gy-3 my-0">
                            <!--begin::Table head-->
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Solusi</th>
                                </tr>
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody class="text-black-300 fw-semibold">
                                @forelse ($penyakit->solusi as $solusi)
                                    <tr class="{{ $loop->iteration % 2 == 0 ? 'odd' : 'even' }}">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $solusi->kode }}</td>
                                        <td>{{ $solusi->solusi }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">Tidak ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <!--end::Table body-->
                        </table>
                    </div>
                    <!--end::Table-->
                </div>
                <!--end: Card Body-->
            </div>
        </div>
        <!--end::Row-->
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('admin.penyakit.destroy', $penyakit) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let btnDelete = document.querySelector('.btn-delete');

        btnDelete.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Hapus Data?',
                text: "Yakin ingin menghapus data penyakit? Data lain yang terkait juga akan dihapus. Tindakan ini tidak dapat dibatalkan.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Hapus',
            }).then((result) => {
                if (result.isConfirmed) {
                    let deleteForm = document.querySelector('#delete-form');

                    deleteForm.submit();
                }
            })
        });
    </script>
@endpush
