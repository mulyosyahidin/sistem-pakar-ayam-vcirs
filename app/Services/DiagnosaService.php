<?php

namespace App\Services;

use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Models\Penyakit;
use PhpParser\Node\Expr\FuncCall;

class DiagnosaService
{
    /**
     * Diagnosa yang akan digunakan
     * 
     * @var \App\Models\Diagnosa
     */
    protected $diagnosa;

    /**
     * Nilai kredit
     * 
     * @var int
     */
    protected $kredit = 1;

    /**
     * Nilai CF User
     * 
     * @var int
     */
    protected $nilaiCfUser = 1;

    /**
     * Create a new instance
     * 
     * Service ini merupakan service yang digunakan untuk melakukan diagnosa
     * menggunakan metode Variable-Centered Intelligent Rule System (VCIRS) dan Certainty Factor (CF).
     * 
     * @param \App\Models\Diagnosa $diagnosa
     * @return void
     */
    public function __construct(Diagnosa $diagnosa)
    {
        $this->diagnosa = $diagnosa;
    }

    /**
     * Melakukan perhitungan diagnosa
     * 
     * Perhitungan diagnosa menggunakan metode Variable-Centered Intelligent Rule System (VCIRS) dan Certainty Factor (CF).
     * Tahapan perhitungan:
     * 0. Membuat rule
     * 1. Menghitung nilai VUR (Variable Utility Rate)
     * 2. Menghitung nilai NUR (Normalized Utility Rate)
     * 3. Menghitung nilai RUR (Relative Utility Rate)
     * 4. Menghitung nilai CF (Certainty Factor)
     * 5. Menghitung nilai CFR (Certainty Factor Result)
     * 6. Menghitung nilai CFR terkombinasi
     * 
     * @return array
     */
    public function hitung()
    {
        $gejalaDiagnosa = $this->diagnosa->gejala()->get()->sortByDesc('bobot');
        $dataPenyakit = Penyakit::with('gejala')->orderBy('kode')->get();
        $matrixNilaiCf = $this->matrixNilaiCf();

        $data = [];

        $i = 0;
        foreach ($dataPenyakit as $penyakit) {
            $gejalaPenyakit = $penyakit->gejala()->get()->sortBy('kode');

            /**
             * Membuat rule
             * 
             * Rule terdiri dari 4 bagian:
             * 1. Gejala
             * 2. Jumlah node
             * 3. Node mana saja yang tidak bernilai null
             * 4. Nomor urutan
             * 
             * @var array
             */
            $rule = [];
            $j = 0;

            foreach ($gejalaPenyakit as $gejala) {
                $rule[$j] = [
                    'gejala' => $gejala,
                    'number_of_node' => countNonNullIndexes($matrixNilaiCf[$gejala->kode]),
                    'nodes' => getNonNullIndexes($matrixNilaiCf[$gejala->kode]),
                    'number' => $j + 1,
                ];

                $j++;
            }

            // total variabel atau gejala yang dimiliki oleh penyakit
            $totalVariabel = count($gejalaPenyakit);

            /**
             * Menghitung nilai VUR (Variable Utility Rate)
             * 
             * VUR = Kredit * (Jumlah node * Nomor urutan / Total variabel)
             * 
             * @var array
             */
            $vur = [];
            $j = 0;

            foreach ($gejalaPenyakit as $gejala) {
                $kredit = $this->kredit;
                $numberOfNode = $rule[$j]['number_of_node'];
                $variabelOrder = $rule[$j]['number'];

                $vur[$j] = [
                    'gejala' => $gejala,
                    'nilai' => $kredit * ($numberOfNode * $variabelOrder / $totalVariabel), // rumus VUR
                ];

                $j++;
            }

            /**
             * Menghitung nilai NUR (Normalized Utility Rate)
             * 
             * NUR = VUR / Jumlah VUR
             * 
             * @var float
             */
            $nur = array_sum(array_column($vur, 'nilai')) / count($vur);

            /**
             * Menghitung nilai RUR (Relative Utility Rate)
             * 
             * RUR = NUR / Jumlah NUR
             * 
             * @var float
             */
            $rur = $nur / count($vur);

            /**
             * Kumpulan jawaban user
             * 
             * @var array
             */
            $jawabanUser = [];
            $j = 0;

            foreach ($gejalaPenyakit as $gejala) {
                $jawabanUser[$j] = [
                    'gejala' => $gejala,
                    'jawaban' => $gejalaDiagnosa->where('kode', $gejala->kode)->first() == null ? '0' : '1', // jika user tidak menjawab maka jawaban dianggap 0, jika menjawab maka jawaban dianggap 1
                ];

                $j++;
            }

            /**
             * Menghitung nilai CF (Certainty Factor) dan CFR (Certainty Factor Result)
             * 
             * CF = Bobot pakar * Bobot user
             * CFR = CF * RUR
             * 
             * @var array
             */
            $nilaiCf = [];
            $j = 0;

            foreach ($gejalaPenyakit as $gejala) {
                $bobotPakar = $gejala->nilai_cf;
                $bobotUser = $jawabanUser[$j]['jawaban'];

                $nilaiCf[$j] = [
                    'gejala' => $gejala,
                    'bobot' => [
                        'pakar' => $bobotPakar,
                        'user' => $bobotUser,
                    ],
                    'nilai' => [
                        'cf' => $bobotPakar * $bobotUser, // rumus CF
                        'cfr' => $bobotPakar * $bobotUser * $rur, // rumus CFR
                    ],
                ];

                $j++;
            }

            /**
             * Menghitung nilai CFR terkombinasi
             * 
             * CFR terkombinasi = CFR1 + CFR2 * (1 - CFR1)
             * Hasilnya kemudian diambil nilai terakhirnya sebagai hasil diagnosa
             * 
             * @var array
             */
            $cfKombinasi = [];
            $j = 0;
            $n = $gejalaPenyakit->count();

            $_current = 0;
            $_next = 0;

            foreach ($gejalaPenyakit as $gejala) {
                if ($j != ($n - 1)) {
                    if ($j == 0) {
                        $_current = $nilaiCf[$j]['nilai']['cfr'];
                        $_next = $nilaiCf[$j + 1]['nilai']['cfr'];
                    } else {
                        $_next = $nilaiCf[$j + 1]['nilai']['cfr'];
                    }

                    $nilai = $_current + $_next * (1 - $_current);
                    $_current = $nilai;

                    // Menambahkan nilai CFR terkombinasi ke array
                    $cfKombinasi[] = $nilai;
                }

                $j++;
            }

            $data[$i] = [
                'penyakit' => $penyakit,
                'rule' => $rule,
                'vur' => $vur,
                'nur' => $nur,
                'rur' => $rur,
                'jawaban_user' => $jawabanUser,
                'nilai_cf' => $nilaiCf,
                'cf_kombinasi' => [
                    'steps' => $cfKombinasi,
                    'result' => end($cfKombinasi),
                    'persentase' => numberFormat(end($cfKombinasi) * 100, 2),
                ]
            ];

            $i++;
        }

        return $data;
    }

    /**
     * Mengambil penyakit tertinggi
     * 
     * @return array
     */
    public function penyakitTertinggi()
    {
        $hasilPerhitungan = $this->hitung();

        $penyakitTertinggi = collect($hasilPerhitungan)->sortByDesc('cf_kombinasi.result')->first();

        return $penyakitTertinggi;
    }

    /**
     * Membentuk matrix nilai CF
     * 
     * @return array
     */
    public function matrixNilaiCf()
    {
        $data = [];

        $dataGejala = Gejala::orderBy('kode')->get();
        $dataPenyakit = Penyakit::with('gejala')->orderBy('kode')->get();

        foreach ($dataGejala as $gejala) {
            $data[$gejala->kode] = [];

            foreach ($dataPenyakit as $penyakit) {
                $data[$gejala->kode][$penyakit->id] = $penyakit->gejala->where('kode', $gejala->kode)->first()?->nilai_cf;
            }
        }

        return $data;
    }
}
