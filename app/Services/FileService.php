<?php

namespace App\Services;

class FileService
{
    /**
     * Mengupload file
     * 
     * @param string $fieldName
     * @return array
     */
    public function upload($fieldName = null)
    {
        if ($fieldName != null) {
            if (request()->has($fieldName) && request()->file($fieldName)->isValid()) {
                $file = request()->file($fieldName);

                $year = date('Y');
                $month = date('m');

                $fileName = time() . '_' . $file->getClientOriginalName();

                $path = "{$year}/{$month}";
                $file->storeAs($path, $fileName, 'uploads');

                return [
                    'file_name' => $fileName,
                    'file_path' => 'uploads/' . $path . '/' . $fileName,
                    'file_size' => $file->getSize(),
                    'mime_type' => $file->getMimeType(),
                ];
            } else {
                return false;
            }
        }
    }

    /**
     * Menghapus file
     * 
     * @param string $filePath
     * @return bool
     */
    public function deleteFile($filePath)
    {
        if (file_exists($filePath)) {
            unlink($filePath);
            return true;
        }

        return false;
    }
}
