<?php

namespace App\Imports;

use App\Models\Data_latih;
use App\Models\Data_latih_gejala;
use App\Models\Gejala;
use App\Models\Penyakit;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DataLatihImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $n = 1;
        foreach ($rows as $row) {
            if (isset($row['penyakit']) && isset($row['gejala'])) {
                $_penyakit = $row['penyakit'];
                $_gejala = $row['gejala'];

                $_dataGejala = explode(',', $_gejala);

                $penyakit = Penyakit::where('kode', $_penyakit)->first();

                $dataLatih = Data_latih::create([
                    'no_kasus' => $n,
                    'id_penyakit' => $penyakit->id,
                ]);

                foreach ($_dataGejala as $_gejala) {
                    $gejala = Gejala::where('kode', $_gejala)->first();

                    $dataLatih->gejala()->create([
                        'id_gejala' => $gejala->id,
                    ]);
                }
            }

            $n++;
        }
    }
}
