<?php

namespace App\Http\Controllers;

use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $count = [
            'penyakit' => Penyakit::count(),
            'gejala' => Gejala::count(),
            'diagnosa' => Diagnosa::count(),
            'user' => User::whereRole('user')->count(),
        ];

        return view('public.home', compact('count'));
    }
}
