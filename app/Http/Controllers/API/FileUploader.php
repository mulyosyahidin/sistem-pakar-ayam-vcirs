<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Http\Request;

class FileUploader extends Controller
{
    /**
     * Upload file to storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $file = $request->file('upload');
            $fileName = time() . '_' . $file->getClientOriginalName();

            $year = date('Y');
            $month = date('m');

            $file->storeAs("{$year}/{$month}", $fileName, 'uploads');

            $picture = File::create([
                'name' => $fileName,
                'file_name' => $fileName,
                'file_path' => 'uploads/' . $year . '/' . $month . '/' . $fileName,
                'mime_type' => $file->getMimeType(),
                'size' => $file->getSize(),
            ]);

            $url = asset($picture->file_path);

            return response()
                ->json([
                    'fileName' => $fileName,
                    'uploaded' => 1,
                    'url' => $url
                ]);
        }
    }
}
