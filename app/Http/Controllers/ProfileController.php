<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile-'. Auth()->user()->role);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email,' . auth()->id()],
            'password' => ['nullable', 'string', 'min:8'],
        ]);

        auth()->user()->update($request->only('name', 'email'));

        if ($request->filled('password')) {
            auth()->user()->update(['password' => bcrypt($request->password)]);
        }

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            if (isset(auth()->user()->media[0])) {
                auth()->user()->media[0]->delete();
            }

            auth()->user()->addMediaFromRequest('picture')->toMediaCollection('profile_picture');
        }

        return redirect()
            ->route('profile.index')
            ->withSuccess('Berhasil memperbarui data profil');
    }

    public function deleteProfilePicture()
    {
        if (isset(auth()->user()->media[0])) {
            auth()->user()->media[0]->delete();
        }

        return redirect()
            ->route('profile.index')
            ->withSuccess('Berhasil menghapus foto profil');
    }
}
