<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $riwayatDiagnosa = Diagnosa::with('penyakit')->where('user_id', auth()->user()->id)->take(5)->get();
        
        return view('user.dashboard', compact('riwayatDiagnosa'));
    }
}
