<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Services\DiagnosaService;
use Illuminate\Http\Request;

class DiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Diagnosa::with('penyakit')->where('user_id', auth()->user()->id)->latest()->paginate();

        return view('user.diagnosa.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $gejala = Gejala::with('kategori')->orderBy('kode')->get();

        $gejala = $gejala->groupBy('id_kategori')->map(function ($item, $key) {
            if ($key == null) {
                $key = 'Lainnya';
            } else {
                $key = $item->first()->kategori->nama;
            }

            return [
                'kategori' => $key,
                'data' => $item,
            ];
        })->sortByDesc(function ($item) {
            return $item['kategori'] === 'Lainnya' ? PHP_INT_MAX : $item['kategori'];
        });

        return view('user.diagnosa.create', compact('gejala'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'gejala' => 'required|array',
        ]);

        $diagnosa = auth()->user()->diagnosa()->create();
        $diagnosa->gejala()->attach($request->gejala);

        $diagnosaService = new DiagnosaService($diagnosa);
        $hasilPerhitungan = $diagnosaService->penyakitTertinggi();

        $diagnosa->update([
            'id_penyakit' => $hasilPerhitungan['penyakit']['id'],
            'persentase' => $hasilPerhitungan['cf_kombinasi']['persentase'],
        ]);

        return redirect()
            ->route('user.diagnosa.show', $diagnosa->id)
            ->withSuccess('Berhasil melakukan diagnosa');
    }

    /**
     * Display the specified resource.
     */
    public function show(Diagnosa $diagnosa)
    {
        return view('user.diagnosa.show', compact('diagnosa'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Diagnosa $diagnosa)
    {
        $diagnosa->delete();

        return redirect()
            ->route('user.diagnosa.index')
            ->withSuccess('Berhasil menghapus riwayat diagnosa');
    }
}
