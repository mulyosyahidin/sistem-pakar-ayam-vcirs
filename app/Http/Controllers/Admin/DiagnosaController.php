<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Services\DiagnosaService;
use Illuminate\Http\Request;

class DiagnosaController extends Controller
{
    public function index()
    {
        $data = Diagnosa::with('user', 'penyakit')->latest()->get();

        return view('admin.diagnosa.index', compact('data'));
    }

    public function show(Diagnosa $diagnosa)
    {
        $diagnosa->loadCount('gejala');

        $diagnosaService = new DiagnosaService($diagnosa);
        $hasilPerhitungan = $diagnosaService->hitung();

        $dataPenyakit = Penyakit::with('gejala')->orderBy('kode')->get();
        $dataGejala = Gejala::orderBy('kode')->get();

        return view('admin.diagnosa.show', compact('diagnosaService', 'dataPenyakit', 'dataGejala', 'diagnosa'));
    }
}
