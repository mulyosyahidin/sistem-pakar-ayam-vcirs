<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Penyakit;
use Illuminate\Http\Request;

class PenyakitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Penyakit::withCount('solusi')->orderBy('kode')->get();

        return view('admin.penyakit.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.penyakit.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required|string|max:4|unique:tb_penyakit,kode',
            'nama' => 'required|string|max:100|unique:tb_penyakit,nama',
            'jenis' => 'nullable|string',
        ]);

        Penyakit::create($request->all());

        return redirect()
            ->route('admin.penyakit.index')
            ->withSuccess('Berhasil menambah data penyakit baru');
    }

    /**
     * Display the specified resource.
     */
    public function show(Penyakit $penyakit)
    {
        $penyakit->loadCount('solusi');

        return view('admin.penyakit.show', compact('penyakit'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penyakit $penyakit)
    {
        return view('admin.penyakit.edit', compact('penyakit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penyakit $penyakit)
    {
        $request->validate([
            'kode' => 'required|string|max:4|unique:tb_penyakit,kode,' . $penyakit->id,
            'nama' => 'required|string|max:100|unique:tb_penyakit,nama,' . $penyakit->id,
            'jenis' => 'nullable|string',
        ]);

        $penyakit->update($request->all());

        return redirect()
            ->route('admin.penyakit.show', $penyakit)
            ->withSuccess('Berhasil memperbarui data penyakit');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Penyakit $penyakit)
    {
        $penyakit->delete();

        return redirect()
            ->route('admin.penyakit.index')
            ->withSuccess('Berhasil menghapus data penyakit');
    }
}
