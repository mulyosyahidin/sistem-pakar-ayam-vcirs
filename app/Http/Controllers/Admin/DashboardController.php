<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Menampilkan halaman dashboard admin.
     * 
     * Dasbor menampilkan ringkasan data aplikasi seperti jumlah penyakit, gejala, user, dan diagnosa
     * serta riwayat diagnosa terbaru.
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $count = [
            'penyakit' => Penyakit::count(),
            'gejala' => Gejala::count(),
            'user' => User::where('role', 'user')->count(),
            'diagnosa' => Diagnosa::count(),
        ];

        $diagnosaTerbaru = Diagnosa::with(['penyakit', 'user'])->latest()->take(5)->get();

        return view('admin.dashboard', compact('count', 'diagnosaTerbaru'));
    }
}
