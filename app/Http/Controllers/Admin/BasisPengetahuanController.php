<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use App\Models\Penyakit;
use Illuminate\Http\Request;

class BasisPengetahuanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $penyakit = Penyakit::with('gejala')->orderBy('kode')->get();

        return view('admin.basis-pengetahuan.index', compact('penyakit'));
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * @param \App\Models\Penyakit $penyakit
     * @return \Illuminate\Http\Response
     */
    public function edit(Penyakit $penyakit)
    {
        $gejala = Gejala::orderBy('kode')->get();
        $gejalaPenyakit = $penyakit->gejala->pluck('id')->toArray();

        return view('admin.basis-pengetahuan.edit', compact('gejala', 'penyakit', 'gejalaPenyakit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penyakit $penyakit)
    {
        $request->validate([
            'gejala'=>'nullable|array',
        ]);

        $penyakit->gejala()->sync($request->gejala);

        return redirect()
            ->route('admin.basis-pengetahuan.index')
            ->withSuccess('Berhasil memperbarui data basis pengetahuan.');
    }
}
