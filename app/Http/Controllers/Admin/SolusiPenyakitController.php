<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Penyakit;
use App\Models\Solusi;
use Illuminate\Http\Request;

class SolusiPenyakitController extends Controller
{
     /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penyakit $penyakit)
    {
        $solusi = Solusi::orderBy('kode')->get();
        $solusiPenyakit = $penyakit->solusi->pluck('id')->toArray();

        return view('admin.solusi-penyakit.edit', compact('penyakit', 'solusi', 'solusiPenyakit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penyakit $penyakit)
    {
        $request->validate([
            'solusi' => 'nullable|array',
        ]);

        $penyakit->solusi()->sync($request->solusi);

        return redirect()
            ->route('admin.penyakit.show', $penyakit)
            ->withSuccess('Berhasil memperbarui data solusi penyakit');
    }
}
