<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Diagnosa extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_diagnosa';

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'id_penyakit',
        'persentase',
    ];

    /**
     * Relasi many-to-many ke model Gejala
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gejala(): BelongsToMany
    {
        return $this->belongsToMany(Gejala::class, 'tb_diagnosa_gejala', 'id_diagnosa', 'id_gejala');
    }

    /**
     * User yang memiliki diagnosa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Penyakit yang dihasilkan dari diagnosa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function penyakit(): BelongsTo
    {
        return $this->belongsTo(Penyakit::class, 'id_penyakit');
    }
}
