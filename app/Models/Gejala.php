<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Gejala extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_gejala';

    /**
     * Tabel tidak memiliki kolom waktu (created_at dan updated_at)
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'id_kategori',
        'kode',
        'nama',
        'nilai_cf',
        'media_type',
        'media_url',
    ];

    /**
     * Relasi many-to-many dengan tabel penyakit
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function penyakit(): BelongsToMany
    {
        return $this->belongsToMany(Penyakit::class, 'tb_basis_pengetahuan', 'id_gejala', 'id_penyakit');
    }

    /**
     * Relasi many-to-one dengan tabel kategori_gejala
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategori(): BelongsTo
    {
        return $this->belongsTo(Kategori_gejala::class, 'id_kategori');
    }
}
