<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, InteractsWithMedia, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Mendapatkan URL Foto Profil Admin
     * 
     * Mendapatkan url foto profil dari media yang terlampir ke model.
     * Jika tidak ada, dapatkan URL dari website ui-avatars.com melalui fungsi getAvatar()
     * 
     * @return string URL Foto Profil
     */
    public function getAvatar(): string
    {
        return isset($this->media[0]) ? $this->media[0]->getFullUrl() : getAvatar($this->name);
    }

    /**
     * Relasi one-to-many ke model Diagnosa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function diagnosa(): HasMany
    {
        return $this->hasMany(Diagnosa::class, 'user_id', 'id');
    }
}
