<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Penyakit extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_penyakit';

    /**
     * Tabel tidak memiliki kolom waktu (created_at dan updated_at)
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'kode',
        'nama',
        'jenis',
    ];

    /**
     * Relasi many-to-many ke model Solusi
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function solusi(): BelongsToMany
    {
        return $this->belongsToMany(Solusi::class, 'tb_solusi_penyakit', 'id_penyakit', 'id_solusi');
    }

    /**
     * Relasi basis pengetahuan
     * 
     * Relasi basis pengetahuan merupakan relasi many-to-many antara model Penyakit dan model Gejala.
     * Relasi ini menggunakan tabel basis pengetahuan (tb_basis_pengetahuan) sebagai pivot table.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gejala(): BelongsToMany
    {
        return $this->belongsToMany(Gejala::class, 'tb_basis_pengetahuan', 'id_penyakit', 'id_gejala');
    }
}
