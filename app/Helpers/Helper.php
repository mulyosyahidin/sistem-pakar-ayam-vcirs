<?php

use App\Models\Data_latih_gejala;
use Illuminate\Support\Facades\DB;

if (!function_exists('activeClass')) {
    /**
     * Return active class if current route is equal to given route
     * 
     * @param array $classes
     * @param string $class
     * @return string
     */
    function activeClass($routes = [], $class = 'show', $queries = [])
    {
        if (is_string($routes)) {
            $routes = [$routes];
        }

        foreach ($routes as $key => $value) {
            if (request()->routeIs($value)) {
                // If current route is equal to given route, return active class
                return $class;
            }
        }

        if (count($queries) > 0) {
            foreach ($queries as $key => $value) {
                if (request()->query($key) == $value) {
                    return $class;
                }
            }
        }
    }
}

if (!function_exists('getAvatar')) {
    /**
     * Return avatar url
     * 
     * Get avatar from https://ui-avatars.com/ by name
     * 
     * @param string $name
     * @return string
     */
    function getAvatar($name = '', $size = 256): string
    {
        $name = trim($name);
        $name = str_replace(' ', '+', $name);
        $url = "https://ui-avatars.com/api/?name=$name&size=$size&background=random&color=fff";

        return $url;
    }
}

if (!function_exists('numberFormat')) {
    /**
     * Format number
     * 
     * Format number with given decimal points, decimal point character, and thousands separator
     * 
     * @param   $number         Number to format
     * @param   $decimals       Number of decimal points
     * @param   $decPoint       Decimal point character
     * @param   $thousandsSep   Thousands separator
     * @return  string
     */
    function numberFormat($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $negation = ($number < 0) ? (-1) : 1;
        $coefficient = 10 ** $decimals;
        $number = $negation * floor((string)(abs($number) * $coefficient)) / $coefficient;

        $result = number_format($number, $decimals, $decPoint, $thousandsSep);

        //get number behind comma
        $numberBehindComma = str_replace($decPoint, '', substr($result, strpos($result, $decPoint) + 1));

        // if number behind comma is 0, remove comma
        if ($numberBehindComma == 0) {
            $result = substr($result, 0, strpos($result, $decPoint));
        }

        return $result;
    }
}

function getNonNullIndexes($array)
{
    $nonNullIndexes = [];

    $key = 1;
    foreach ($array as $value) {
        if ($value !== null) {
            $nonNullIndexes[] = $key;
        }

        $key++;
    }

    return $nonNullIndexes;
}

function countNonNullIndexes($array)
{
    return count(array_filter($array, function ($value) {
        return $value !== null;
    }));
}

function getYouTubeEmbedURL($youTubeUrl)
{
    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

    if (preg_match($longUrlRegex, $youTubeUrl, $matches)) {
        $youTubeUrl = 'https://www.youtube.com/embed/' . $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $youTubeUrl, $matches)) {
        $youTubeUrl = 'https://www.youtube.com/embed/' . $matches[count($matches) - 1];
    }

    return $youTubeUrl;
}