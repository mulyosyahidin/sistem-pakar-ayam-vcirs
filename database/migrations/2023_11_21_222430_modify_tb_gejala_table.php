<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->dropColumn(['tipe_media', 'media', 'sumber']);

            $table->decimal('nilai_cf', 8, 2)->nullable()->after('nama');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->enum('tipe_media', ['gambar', 'video', 'link'])->nullable()->after('nama');
            $table->string('media')->nullable()->after('tipe_media')->nullable();
            $table->string('sumber')->after('media')->nullable();

            $table->dropColumn('nilai_cf');
        });
    }
};
