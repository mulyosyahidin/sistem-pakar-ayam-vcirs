<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tb_diagnosa', function (Blueprint $table) {
            $table->dropColumn(['nama', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tb_diagnosa', function (Blueprint $table) {
            $table->string('nama')->after('user_id');
            $table->boolean('status')->default(false)->after('nama');
        });
    }
};
