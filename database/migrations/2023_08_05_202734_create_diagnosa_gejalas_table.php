<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_diagnosa_gejala', function (Blueprint $table) {
            $table->unsignedInteger('id_diagnosa')->nullable();
            $table->unsignedInteger('id_gejala')->nullable();

            $table->foreign('id_diagnosa')->references('id')->on('tb_diagnosa')->onDelete('cascade');
            $table->foreign('id_gejala')->references('id')->on('tb_gejala')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_diagnosa_gejala');
    }
};
