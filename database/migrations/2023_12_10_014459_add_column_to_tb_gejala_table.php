<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->enum('media_type', ['image', 'video'])->nullable();
            $table->string('media_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->dropColumn('media_type');
            $table->dropColumn('media_url');
        });
    }
};
