<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('tb_solusi_penyakit');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::create('tb_solusi_penyakit', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('id_penyakit')->nullable();
            $table->string('kode', 4);
            $table->mediumText('solusi');

            $table->foreign('id_penyakit')->references('id')->on('tb_penyakit')->onDelete('cascade');
        });
    }
};
