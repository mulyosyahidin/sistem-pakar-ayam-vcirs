<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_penyakit', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('kode', 4);
            $table->string('nama', 100);
            $table->string('jenis')->nullable();
            $table->mediumText('deskripsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_penyakit');
    }
};
