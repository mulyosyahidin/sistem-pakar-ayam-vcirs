<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->unsignedInteger('id_kategori')->nullable()->after('id');

            $table->foreign('id_kategori')->references('id')->on('tb_kategori_gejala')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tb_gejala', function (Blueprint $table) {
            $table->dropForeign(['id_kategori']);

            $table->dropColumn('id_kategori');
        });
    }
};
