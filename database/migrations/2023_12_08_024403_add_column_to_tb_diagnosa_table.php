<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tb_diagnosa', function (Blueprint $table) {
            $table->unsignedInteger('id_penyakit')->nullable()->after('user_id');
            $table->decimal('persentase', 5, 2)->nullable()->after('id_penyakit');

            $table->foreign('id_penyakit')->references('id')->on('tb_penyakit')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tb_diagnosa', function (Blueprint $table) {
            $table->dropForeign(['id_penyakit']);
            
            $table->dropColumn(['id_penyakit', 'persentase']);
        });
    }
};
