<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_solusi_penyakit', function (Blueprint $table) {
            $table->unsignedInteger('id_solusi')->nullable();
            $table->unsignedInteger('id_penyakit')->nullable();

            $table->foreign('id_solusi')->references('id')->on('tb_solusi')->onDelete('cascade');
            $table->foreign('id_penyakit')->references('id')->on('tb_penyakit')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_solusi_penyakit');
    }
};
