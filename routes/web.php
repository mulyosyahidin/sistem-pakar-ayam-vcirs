<?php

use App\Http\Controllers\Admin\BasisPengetahuanController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DiagnosaController as AdminDiagnosaController;
use App\Http\Controllers\Admin\GejalaController;
use App\Http\Controllers\Admin\KategoriGejalaController;
use App\Http\Controllers\Admin\PenyakitController;
use App\Http\Controllers\Admin\SolusiController;
use App\Http\Controllers\Admin\SolusiPenyakitController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\User\DashboardController as UserDashboardController;
use App\Http\Controllers\User\DiagnosaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth', 'role:admin'], 'as' => 'admin.', 'prefix' => 'admin'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('kategori-gejala', KategoriGejalaController::class)->except('show');
    Route::resource('gejala', GejalaController::class);
    Route::resource('penyakit', PenyakitController::class);
    Route::resource('solusi', SolusiController::class);

    Route::get('/penyakit/{penyakit}/solusi', [SolusiPenyakitController::class, 'edit'])->name('penyakit.solusi.edit');
    Route::put('/penyakit/{penyakit}/solusi', [SolusiPenyakitController::class, 'update'])->name('penyakit.solusi.update');

    Route::get('/basis-pengetahuan', [BasisPengetahuanController::class, 'index'])->name('basis-pengetahuan.index');
    Route::get('/basis-pengetahuan/{penyakit}/edit', [BasisPengetahuanController::class, 'edit'])->name('basis-pengetahuan.edit');
    Route::put('/basis-pengetahuan/{penyakit}', [BasisPengetahuanController::class, 'update'])->name('basis-pengetahuan.update');

    Route::resource('diagnosa', AdminDiagnosaController::class)->only(['index', 'show', 'destroy']);
    Route::resource('users', UserController::class)->only(['index', 'show', 'destroy']);
});

Route::group(['middleware' => ['auth', 'role:user'], 'as' => 'user.', 'prefix' => 'user'], function () {
    Route::get('/dashboard', [UserDashboardController::class, 'index'])->name('dashboard');

    Route::resource('diagnosa', DiagnosaController::class)->except(['edit', 'update']);
});

Route::group(['middleware' => ['auth'], 'as' => 'profile.', 'prefix' => 'profile'], function () {
    Route::get('/', [ProfileController::class, 'index'])->name('index');
    Route::put('/', [ProfileController::class, 'update'])->name('update');
    Route::delete('/delete-profile-picture', [ProfileController::class, 'deleteProfilePicture'])->name('delete-profile-picture');
});

require __DIR__ . '/auth.php';
